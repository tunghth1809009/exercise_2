#include <stdio.h>

#include <stdio.h>
#include <mem.h>

char a[50];
float dema = 0;
float deme = 0;
float demi = 0;
float demo = 0;
float demu = 0;
float pta, pte, pti, pto, ptu, ptconlai;

void dem() {
    for (int i = 0; i < strlen(a); ++i) {
        if (a[i] == 'a' || a[i] == 'A') {
            dema++;
        }
        if (a[i] == 'e' || a[i] == 'E') {
            deme++;
        }
        if (a[i] == 'i' || a[i] == 'I') {
            demi++;
        }
        if (a[i] == 'o' || a[i] == 'O') {
            demo++;
        }
        if (a[i] == 'u' || a[i] == 'U') {
            demu++;
        }
    }
}

void in() {
    printf("a xuat hien %.0f lan\n", dema);
    printf("e xuat hien %.0f lan\n", deme);
    printf("i xuat hien %.0f lan\n", demi);
    printf("o xuat hien %.0f lan\n", demo);
    printf("u xuat hien %.0f lan\n", demu);
}

void phantram() {
    pta = (dema / strlen(a)) * 100;
    pte = (deme / strlen(a)) * 100;
    pti = (demi / strlen(a)) * 100;
    pto = (demo / strlen(a)) * 100;
    ptu = (demu / strlen(a)) * 100;
    ptconlai = 100 - pta - pte - pti - ptu - pto;
}
void inraphantram()
{

    printf("Phan tram ki tu trong tong so chuoi la\n");
    printf("a : %.1f %%\n",pta);
    printf("e : %.1f %%\n",pte);
    printf("i : %.1f %%\n",pti);
    printf("o : %.1f %%\n",pto);
    printf("u : %.1f %%\n",ptu);
    printf("cac ki tu con lai %.1f %%\n",ptconlai);
}

int main() {
    printf("Enter String");
    fgets(a, sizeof(a) * sizeof(char), stdin);
    if (!strchr(a, '\n')) {
        while (fgetc(stdin) != '\n');
    }
    dem();
    in();
    phantram();
    inraphantram();
    return 0;
}